################################
####### Private DNS Zone #######
################################

# Create private DNS zone
resource "azurerm_private_dns_zone" "aks" {
  name                = "privatelink.${var.resource_group_location}.azmk8s.io"
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  tags                = local.common_tags

  lifecycle {
    ignore_changes = [
      number_of_record_sets
    ]
  }
}

################################
####### Managed Identity #######
################################

# Create User Assigned Identity
resource "azurerm_user_assigned_identity" "aks" {
  name                = "uai-aks-${var.prefix}-${var.environment}"
  location            = azurerm_resource_group.aks_resource_group.location
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  tags                = local.common_tags
}

################################
############# RBAC #############
################################

# Role assignment to be able to manage private DNS zones
resource "azurerm_role_assignment" "aks_private_dns" {
  scope                            = azurerm_private_dns_zone.aks.id
  role_definition_name             = "Private DNS Zone Contributor"
  principal_id                     = azurerm_user_assigned_identity.aks.principal_id
  skip_service_principal_aad_check = true
}

# Role assignment to be able to manage the AKS subnet
resource "azurerm_role_assignment" "aks_subnet" {
  scope                            = azurerm_resource_group.aks_resource_group.id
  role_definition_name             = "Network Contributor"
  principal_id                     = azurerm_user_assigned_identity.aks.principal_id
  skip_service_principal_aad_check = true
  #principal_id         = azurerm_kubernetes_cluster.aks_cluster.identity[0].principal_id
}

# Role assignment to publish metrics
resource "azurerm_role_assignment" "aks-prod-metrics-publisher" {
  scope                            = azurerm_kubernetes_cluster.aks_cluster.id
  role_definition_name             = "Monitoring Metrics Publisher"
  principal_id                     = azurerm_user_assigned_identity.aks.principal_id
  skip_service_principal_aad_check = true
}

# Role assignment to pull images from ACR
resource "azurerm_role_assignment" "aks_acr" {
  scope                            = azurerm_container_registry.aks_acr.id
  role_definition_name             = "AcrPull"
  principal_id                     = azurerm_user_assigned_identity.aks.principal_id
  skip_service_principal_aad_check = true
  #principal_id         = azurerm_kubernetes_cluster.aks_cluster.kubelet_identity[0].object_id
}

# Role assignment to manage App Gateway
#resource "azurerm_role_assignment" "aks_appgateway" {
#  scope                = azurerm_application_gateway.aks_ag.id
#  role_definition_name = "Contributor"
#  principal_id         = azurerm_user_assigned_identity.aks.principal_id
#}


################################
############# AKS ##############
################################

# Create Aks Cluster
resource "azurerm_kubernetes_cluster" "aks_cluster" {
  depends_on                = [azurerm_role_assignment.aks_private_dns]
  name                      = var.cluster_name == null ? "aks0${var.prefix}0${var.environment}" : var.cluster_name
  kubernetes_version        = var.kubernetes_version
  location                  = azurerm_resource_group.aks_resource_group.location
  resource_group_name       = azurerm_resource_group.aks_resource_group.name
  dns_prefix                = var.prefix
  private_cluster_enabled   = var.private_cluster_enabled
  private_dns_zone_id       = azurerm_private_dns_zone.aks.id
  sku_tier                  = var.sku_tier
  automatic_channel_upgrade = var.automatic_channel_upgrade
  tags                      = merge(local.common_tags, var.tags)

  default_node_pool {
    orchestrator_version   = var.kubernetes_version
    name                   = "${var.agents_pool_name}0${var.prefix}0${var.environment}"
    node_count             = var.agents_count
    vm_size                = var.agents_size
    os_disk_size_gb        = var.os_disk_size_gb
    vnet_subnet_id         = azurerm_subnet.aks_subnet.id
    enable_auto_scaling    = var.enable_auto_scaling == true ? var.enable_auto_scaling : false
    max_count              = var.enable_auto_scaling == true ? var.agents_max_count : null
    min_count              = var.enable_auto_scaling == true ? var.agents_min_count : null
    enable_node_public_ip  = var.enable_node_public_ip
    availability_zones     = var.agents_availability_zones
    node_labels            = var.agents_labels
    type                   = var.agents_type
    tags                   = merge(local.common_tags, var.tags, var.agents_tags)
    max_pods               = var.agents_max_pods
    enable_host_encryption = var.enable_host_encryption
  }

  lifecycle {
    ignore_changes = [
      default_node_pool[0].node_count
    ]
  }

  auto_scaler_profile {
    max_unready_nodes   = var.enable_auto_scaling == true ? var.max_unready_nodes : null
    scale_down_unneeded = var.enable_auto_scaling == true ? var.scale_down_unneeded : null
  }

  network_profile {
    network_plugin     = var.network_plugin
    network_policy     = var.network_policy
    service_cidr       = var.net_profile_service_cidr
    pod_cidr           = var.net_profile_pod_cidr
    dns_service_ip     = var.net_profile_dns_service_ip
    outbound_type      = var.net_profile_outbound_type
    load_balancer_sku  = var.load_balancer_sku
    docker_bridge_cidr = var.net_profile_docker_bridge_cidr
  }

  identity {
    type                      = var.identity_type
    user_assigned_identity_id = azurerm_user_assigned_identity.aks.id
  }

  role_based_access_control {
    enabled = var.enable_role_based_access_control

    azure_active_directory {
      managed                = var.rbac_aad_managed
      tenant_id              = data.azurerm_client_config.current.tenant_id
      admin_group_object_ids = [azuread_group.aks_admins.object_id]
      azure_rbac_enabled     = var.azure_rbac_enabled
    }
  }

  addon_profile {
    azure_policy {
      enabled = var.enable_azure_policy
    }
    http_application_routing {
      enabled = var.enable_http_application_routing
    }
    kube_dashboard {
      enabled = var.enable_kube_dashboard
    }
    oms_agent {
      enabled                    = var.enable_oms_agent
      log_analytics_workspace_id = var.enable_oms_agent == true ? azurerm_log_analytics_workspace.aks_law.id : null
    }
    ingress_application_gateway {
      enabled = var.enable_ingress_application_gateway
      #gateway_id = var.enable_ingress_application_gateway == true ? azurerm_application_gateway.aks_ag.id : null
    }
  }
}

################################
####### Azure Key Vault ########
################################

# write AKS FQDN to AKV
resource "azurerm_key_vault_secret" "aks_fqdn" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-fqdn"
  value        = azurerm_kubernetes_cluster.aks_cluster.fqdn
  content_type = "url"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}

# write AKS Client Cert to AKV
resource "azurerm_key_vault_secret" "aks_client_cert" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-client-cert"
  value        = azurerm_kubernetes_cluster.aks_cluster.kube_admin_config.0.client_certificate
  content_type = "certificate"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}

# write AKS Client Key to AKV
resource "azurerm_key_vault_secret" "aks_client_key" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-client-key"
  value        = azurerm_kubernetes_cluster.aks_cluster.kube_admin_config.0.client_key
  content_type = "key"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}

# write AKS CA Cert to AKV
resource "azurerm_key_vault_secret" "aks_ca_cert" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-ca-cert"
  value        = azurerm_kubernetes_cluster.aks_cluster.kube_admin_config.0.cluster_ca_certificate
  content_type = "certificate"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}
