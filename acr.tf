################################
####### Private DNS Zone #######
################################

# Private DNS for ACR
resource "azurerm_private_dns_zone" "aks_acr" {
  name                = "privatelink.azurecr.io"
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  tags                = local.common_tags

  lifecycle {
    ignore_changes = [
      number_of_record_sets
    ]
  }
}

################################
####### Managed Identity #######
################################

# Create User Assigned Identity
resource "azurerm_user_assigned_identity" "aks_acr" {
  name                = "uai-acr-${var.prefix}-${var.environment}"
  location            = azurerm_resource_group.aks_resource_group.location
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  tags                = local.common_tags
}

################################
### Azure Container Registry ###
################################

# Create ACR
resource "azurerm_container_registry" "aks_acr" {
  name                          = "acr0aks0${var.prefix}0${var.environment}"
  location                      = azurerm_resource_group.aks_resource_group.location
  resource_group_name           = azurerm_resource_group.aks_resource_group.name
  sku                           = var.acr_sku
  admin_enabled                 = var.acr_admin_enabled
  public_network_access_enabled = false
  tags                          = local.common_tags

  identity {
    type = var.acr_identity_type
    identity_ids = [
      azurerm_user_assigned_identity.aks_acr.id
    ]
  }
}

################################
######## Private Link ##########
################################

# Private Endpoint fo ACR
resource "azurerm_private_endpoint" "aks_acr" {
  name                = "${var.prefix}-${var.environment}-pvep-acr"
  location            = azurerm_resource_group.aks_resource_group.location
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  subnet_id           = azurerm_subnet.aks_subnet.id
  tags                = local.common_tags
  private_service_connection {
    name                           = "acr-privateserviceconnection"
    private_connection_resource_id = azurerm_container_registry.aks_acr.id
    is_manual_connection           = false
    subresource_names              = ["registry"]
  }

  private_dns_zone_group {
    name                 = azurerm_private_dns_zone.aks_acr.name
    private_dns_zone_ids = [azurerm_private_dns_zone.aks_acr.id]
  }
}

# DNS link for ACR
resource "azurerm_private_dns_zone_virtual_network_link" "aks_acr" {
  name                  = "${var.prefix}-${var.environment}-dnslink-acr"
  resource_group_name   = azurerm_resource_group.aks_resource_group.name
  private_dns_zone_name = azurerm_private_dns_zone.aks_acr.name
  virtual_network_id    = azurerm_virtual_network.aks_vnet.id
  tags                  = local.common_tags
}

################################
####### Azure Key Vault ########
################################

resource "azurerm_key_vault_secret" "acr_admin_user" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-acr-admin-user"
  value        = azurerm_container_registry.aks_acr.admin_username
  content_type = "username"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}

resource "azurerm_key_vault_secret" "acr_admin_password" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-acr-admin-password"
  value        = azurerm_container_registry.aks_acr.admin_password
  content_type = "password"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}

resource "azurerm_key_vault_secret" "acr_url" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-acr-url"
  value        = azurerm_container_registry.aks_acr.login_server
  content_type = "url"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}
