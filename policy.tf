resource "azurerm_resource_policy_assignment" "K8sAzureBlockDefault" {
  name                 = "K8sAzureBlockDefault"
  display_name         = "K8sAzureBlockDefault"
  resource_id          = azurerm_kubernetes_cluster.aks_cluster.id
  policy_definition_id = "/providers/Microsoft.Authorization/policyDefinitions/9f061a12-e40d-4183-a00e-171812443373"
  enforce              = true
  parameters           = <<PARAMETERS
  {
    "effect": {
      "value": "deny"
    }
  }
PARAMETERS
}

resource "azurerm_resource_policy_assignment" "K8sAzureBlockAutomountToken" {
  name                 = "K8sAzureBlockAutomountToken"
  display_name         = "K8sAzureBlockAutomountToken"
  resource_id          = azurerm_kubernetes_cluster.aks_cluster.id
  policy_definition_id = "/providers/microsoft.authorization/policydefinitions/423dd1ba-798e-40e4-9c4d-b6902674b423"
  enforce              = true
  parameters           = <<PARAMETERS
  {
    "effect": {
      "value": "deny"
    }
  }
PARAMETERS
}

resource "azurerm_resource_policy_assignment" "k8s_policy_baseline" {
  name                 = "Kubernetes cluster pod security baseline standards for Linux-based workloads"
  display_name         = "Kubernetes cluster pod security baseline standards for Linux-based workloads"
  resource_id          = azurerm_kubernetes_cluster.aks_cluster.id
  policy_definition_id = "/providers/Microsoft.Authorization/policySetDefinitions/a8640138-9b0a-4a28-b8cb-1666c838647d"
  enforce              = true
  parameters           = <<PARAMETERS
  {
    "effect": {
      "value": "deny"
    }
  }
PARAMETERS
}

resource "azurerm_resource_policy_assignment" "k8s_policy_restricted" {
  name                 = "Kubernetes cluster pod security restricted standards for Linux-based workloads"
  display_name         = "Kubernetes cluster pod security restricted standards for Linux-based workloads"
  resource_id          = azurerm_kubernetes_cluster.aks_cluster.id
  policy_definition_id = "/providers/Microsoft.Authorization/policySetDefinitions/42b8ef37-b724-4e24-bbc8-7a7708edfe00"
  enforce              = true
  parameters           = <<PARAMETERS
  {
    "effect": {
      "value": "audit"
    }
  }
PARAMETERS
}
