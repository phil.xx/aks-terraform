################################
######## Azure Monitor #########
################################

resource "azurerm_monitor_diagnostic_setting" "aks" {
  name                       = "diag-aks-${var.prefix}-${var.environment}"
  target_resource_id         = azurerm_kubernetes_cluster.aks_cluster.id
  storage_account_id         = azurerm_storage_account.aks_stac.id
  log_analytics_workspace_id = azurerm_log_analytics_workspace.aks_law.id

  log {
    category = "cluster-autoscaler"
    enabled  = false

    retention_policy {
      enabled = false
      days    = 0
    }
  }

  log {
    category = "guard"
    enabled  = false

    retention_policy {
      enabled = false
      days    = 0
    }
  }

  log {
    category = "kube-apiserver"
    enabled  = false

    retention_policy {
      enabled = false
      days    = 0
    }
  }

  log {
    category = "kube-audit"
    enabled  = true

    retention_policy {
      enabled = true
      days    = var.log_retention_in_days
    }
  }

  log {
    category = "kube-audit-admin"
    enabled  = true

    retention_policy {
      enabled = true
      days    = var.log_retention_in_days
    }
  }

  log {
    category = "kube-controller-manager"
    enabled  = false

    retention_policy {
      enabled = false
      days    = 0
    }
  }

  log {
    category = "kube-scheduler"
    enabled  = false

    retention_policy {
      enabled = false
      days    = 0
    }
  }

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = true
      days    = var.log_retention_in_days
    }
  }
}
