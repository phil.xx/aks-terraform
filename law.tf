
################################
### Log Analytics Workspace ####
################################

#
resource "azurerm_log_analytics_workspace" "aks_law" {
  name                = "law-${var.prefix}-${var.environment}"
  location            = azurerm_resource_group.aks_resource_group.location
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  sku                 = var.log_analytics_workspace_sku
  retention_in_days   = var.log_retention_in_days
  daily_quota_gb      = var.log_daily_quota_gb
  tags                = local.common_tags
}

################################
####### Azure Key Vault ########
################################

# write SAS key to key vault
resource "azurerm_key_vault_secret" "law_prim_key" {
  depends_on   = [azurerm_role_assignment.aks_akv_tf]
  name         = "${var.prefix}-${var.environment}-law-primary-shared-key"
  value        = azurerm_log_analytics_workspace.aks_law.primary_shared_key
  content_type = "key"
  key_vault_id = azurerm_key_vault.aks_akv.id
  tags         = local.common_tags
}

