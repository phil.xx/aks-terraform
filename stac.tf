# Random Storage Account Name
resource "random_string" "storage_account" {
  length  = 7
  special = false
  lower   = true
  upper   = false
}

################################
####### Private DNS Zone #######
################################

################################
####### Storage Account ########
################################

resource "azurerm_storage_account" "aks_stac" {
  name                      = "stac0aks0${var.prefix}0${var.environment}0${random_string.storage_account.id}"
  resource_group_name       = azurerm_resource_group.aks_resource_group.name
  location                  = azurerm_resource_group.aks_resource_group.location
  enable_https_traffic_only = var.enable_https_traffic_only
  min_tls_version           = var.min_tls_version
  account_kind              = var.storage_account_kind
  account_tier              = var.storage_account_tier
  account_replication_type  = var.storage_account_replication_type
  access_tier               = var.storage_account_access_tier
  tags                      = local.common_tags
}

################################
######## Private Link ##########
################################
