terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.70.0"
    }
    azuread = {
      source  = "hashicorp/azuread"
      version = "1.6.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }

  required_version = "1.0.3"
}

provider "azurerm" {
  features {}
}

provider "azuread" {
  use_microsoft_graph = true
}
