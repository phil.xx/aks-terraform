################################
######## Virtual Netwrk ########
################################

resource "azurerm_virtual_network" "aks_vnet" {
  name                = "vnet-aks-${var.prefix}-${var.environment}"
  address_space       = var.address_space
  location            = azurerm_resource_group.aks_resource_group.location
  resource_group_name = azurerm_resource_group.aks_resource_group.name
  tags                = local.common_tags
}

################################
############ Subnet ############
################################

resource "azurerm_subnet" "aks_subnet" {
  name                                           = "snet-aks-${var.prefix}-${var.environment}"
  resource_group_name                            = azurerm_resource_group.aks_resource_group.name
  virtual_network_name                           = azurerm_virtual_network.aks_vnet.name
  address_prefixes                               = var.aks_address_prefixes
  enforce_private_link_endpoint_network_policies = true
  service_endpoints                              = ["Microsoft.ContainerRegistry", "Microsoft.KeyVault"]
}

resource "azurerm_subnet" "ag_subnet" {
  name                                           = "snet-ag-${var.prefix}-${var.environment}"
  resource_group_name                            = azurerm_resource_group.aks_resource_group.name
  virtual_network_name                           = azurerm_virtual_network.aks_vnet.name
  address_prefixes                               = var.ag_address_prefixes
  enforce_private_link_endpoint_network_policies = true
  service_endpoints                              = ["Microsoft.ContainerRegistry", "Microsoft.KeyVault"]
}

resource "azurerm_subnet" "bastian_subnet" {
  name                                           = "snet-bastion-${var.prefix}-${var.environment}"
  resource_group_name                            = azurerm_resource_group.aks_resource_group.name
  virtual_network_name                           = azurerm_virtual_network.aks_vnet.name
  address_prefixes                               = var.bastion_address_prefixes
  enforce_private_link_endpoint_network_policies = true
  service_endpoints                              = ["Microsoft.ContainerRegistry", "Microsoft.KeyVault"]
}
