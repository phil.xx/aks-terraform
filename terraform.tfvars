# Cluster managed by
managedby = "Terraform"

# defines stage
environment = "stg"

# defines stage
owner = "pwe"

# defines resource prefix
prefix = "pwe"

# defines AKS version
kubernetes_version = "1.21.2"

#
enable_auto_scaling = true

#
max_unready_nodes = 1

#
agents_min_count = 2

#
agents_max_count = 5

#
agents_labels = {
  "scope" = "system"
}

# defines used CNI for AKS
network_plugin = "azure"

# defines network policy
network_policy = "azure"

#
address_space = ["10.0.0.0/16"]

#
aks_address_prefixes = ["10.0.1.0/24"]

#
ag_address_prefixes = ["10.0.10.0/24"]

#
bastion_address_prefixes = ["10.0.2.0/24"]

#
net_profile_service_cidr = "10.96.0.0/12"

# DNS service IP (needs to be part of the subnet)
net_profile_dns_service_ip = "10.96.0.10"

# defines container bridge address space
net_profile_docker_bridge_cidr = "10.112.0.1/16"

# enable or disable logging for AKS
enable_oms_agent = true

# Enalbe Azure Polcies for AKS
enable_azure_policy = true

# Enables RBAC for Azure Key Vault
enable_rbac_authorization = true

# Enable the Admin user for the ACR
acr_admin_enabled = true

# Set Cluster in private mode
private_cluster_enabled = true

#
acr_identity_type = "UserAssigned"

#
acr_sku = "Premium"

#
enable_ingress_application_gateway = false
