locals {
  # Common tags to be assigned to all resources
  common_tags = {
    environment = var.environment
    managedBy   = var.managedby
    owner       = var.owner
  }
}

# get current subscription
data "azurerm_subscription" "current" {
}

# get current client
data "azurerm_client_config" "current" {
}

### IAM / RBAC ###

data "azuread_user" "pwe" {
  user_principal_name = "philip.welz@whiteduck.de"
}

data "azuread_user" "nme" {
  user_principal_name = "nico.meisenzahl@whiteduck.de"
}

# Create Azure AD Group for Azure Key Vault
resource "azuread_group" "akv_admins" {
  display_name = "kv-${var.prefix}-${var.environment}-admins"
  description  = "Azure Key Vault Admin Group"
  members = [
    data.azuread_user.pwe.object_id,
    data.azuread_user.nme.object_id
  ]
}

# Create Azure AD Group for Azure Kubernetes
resource "azuread_group" "aks_admins" {
  display_name = "aks-${var.prefix}-${var.environment}-admins"
  description  = "Azure Kubernetes Admin Group"
  members = [
    data.azuread_user.pwe.object_id,
    data.azuread_user.nme.object_id
  ]
}

### Resource Groups ###

# Create AKS Resource Group
resource "azurerm_resource_group" "aks_resource_group" {
  name     = "rg-aks-${var.prefix}-${var.environment}"
  location = var.resource_group_location
  tags     = local.common_tags
}

