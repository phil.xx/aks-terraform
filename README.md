# AKS-Terraform

## Docs

[Azure Credentials](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret)

[Pipeline](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Terraform/Base.latest.gitlab-ci.yml)

```shell
az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/d47fae4a-a9d1-44f7-a09e-23bb76696838"
```

## Project Variables

Variable            | Spalte 2
------------------- | --------
ARM_CLIENT_ID       | Inhalt
ARM_CLIENT_SECRET   | Inhalt
ARM_SUBSCRIPTION_ID |
ARM_TENANT_ID       |


# Todo

* azurerm_monitor_diagnostic_setting
* azurerm_log_analytics_workspace
