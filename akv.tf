################################
####### Azure Key Vault ########
################################

resource "azurerm_key_vault" "aks_akv" {
  name                            = "aks-kv-${var.prefix}-${var.environment}"
  location                        = azurerm_resource_group.aks_resource_group.location
  resource_group_name             = azurerm_resource_group.aks_resource_group.name
  tenant_id                       = data.azurerm_subscription.current.tenant_id
  enabled_for_disk_encryption     = var.enabled_for_disk_encryption
  enabled_for_template_deployment = var.enabled_for_template_deployment
  enabled_for_deployment          = var.enabled_for_deployment
  purge_protection_enabled        = var.purge_protection_enabled
  enable_rbac_authorization       = var.enable_rbac_authorization
  soft_delete_retention_days      = var.key_vault_retention_days
  sku_name                        = var.key_vault_sku
  tags                            = local.common_tags

  network_acls {
    default_action = "Allow"
    bypass         = "AzureServices"
  }
}

################################
############# RBAC #############
################################

# Role assignment for Terraform service principal to write secrets
resource "azurerm_role_assignment" "aks_akv_tf" {
  scope                = azurerm_key_vault.aks_akv.id
  role_definition_name = "Key Vault Secrets Officer"
  principal_id         = data.azurerm_client_config.current.object_id
}

# Role assignment for AKV Admin group
resource "azurerm_role_assignment" "aks_akv_admin" {
  scope                = azurerm_key_vault.aks_akv.id
  role_definition_name = "Key Vault Secrets Officer"
  principal_id         = azuread_group.akv_admins.object_id
}
